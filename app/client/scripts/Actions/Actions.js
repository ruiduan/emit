(function() {
  'use strict';
angular.module('app.actions', ['ngRoute','angular-loading-bar','ngCookies'])
.controller('mainCtrl', ['$scope', '$route', '$routeParams',function($scope, $route, $routeParams) {
  $scope.$route = $route;
  $scope.$routeParams = $routeParams;
  console.log ($routeParams);
  if(angular.isDefined($routeParams.id)){
  	$scope.id = $routeParams.id;
  }
  else{
  	$scope.id = 0;
  }
  if(angular.isDefined($routeParams.type)){
  	$scope.type = $routeParams.type;
  }
  else{
  	$scope.type = "reply";
  }
  $scope.template = {url: "views/actions/" + $scope.type + ".html"};
  
}])
.controller('CompleteController', ['$scope', '$route', '$routeParams', '$location','$http','$templateCache','$modal','$cookies','employeeFactory','emitFactory',
  function($scope, $route, $routeParams, $location,$http,$templateCache,$modal,$cookies,employeeFactory,emitFactory) {
  $scope.$route = $route;
  $scope.$location = $location;
  $scope.$routeParams = $routeParams;
  $scope.bu = $routeParams.bu;
  if(angular.isDefined($routeParams.id)){
  	$scope.id = $routeParams.id;
  }
  else{
  	$scope.id = 0;
  }
  
  //get employee/link list
  $scope.employees = [];
 
  function refreshItems(){
    employeeFactory.getEmployees()
    .then(function(data){
      $scope.employees = data;
      emitFactory.getLinks($scope.id)
	  .then(function(data){
	    $scope.links = data;
	  },
	  function(errorMessage){      
	  	$scope.error=errorMessage;
	  })
    },
    function(errorMessage){
      $scope.error=errorMessage;
    })
    ;
  };
  
  refreshItems();
  var open = function (message) {
    var modalInstance = $modal.open({
      templateUrl: 'modalContent.html',
      controller: 'ModalInstanceCtrl',
      resolve: {
        message: function () {
          return message;
        }
      }
    });
	
    modalInstance.result.then(function () {
      console.log('Modal closed at: ' + new Date());
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };
  
  $scope.submit = function(){
  	var complete_url = 'http://intranet.ait.au/developer/emit/services/complete_action.cfm?key=3m1tr0b0trun';

  	var requestData = {
      id: $scope.id,
      type: $scope.type,
      reference_po_customer: $scope.reference_po_customer,
      reference: $scope.reference,
      closure_details: $scope.closure_details,
      email_customer: $scope.email_customer,
      notify_internal_person: $scope.notify_internal_person
    };
  	$http.post(complete_url, 
  	  $.param(requestData),
		  {
      headers:
      {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
      }
	).
	success(function($data, $status){
      console.log($data);
      if($.trim($data) == 'success' ){
        open('Item has been completed successfully.');
        $location.path('/details/' + $scope.id);
      }
    }).
    error(function(data, status) {
      $scope.data = data || "Request failed";
      $scope.status = status;
    })
  };
  
}])
.controller('DeleteController', ['$scope', '$route', '$routeParams', '$location','$http','$templateCache','$modal','$cookies','employeeFactory','emitFactory',
function($scope, $route, $routeParams, $location,$http,$templateCache,$modal,$cookies,employeeFactory,emitFactory) {
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;
    $scope.bu = $routeParams.bu;
    if(angular.isDefined($routeParams.id)){
      $scope.id = $routeParams.id;
    }
    else{
      $scope.id = 0;
    }

    //get employee/link list
    $scope.employees = [];

    function refreshItems(){
      employeeFactory.getEmployees()
        .then(function(data){
          $scope.employees = data;
        },
        function(errorMessage){
          $scope.error=errorMessage;
        })
      ;
    };

    refreshItems();
    var open = function (message) {
      var modalInstance = $modal.open({
        templateUrl: 'modalContent.html',
        controller: 'ModalInstanceCtrl',
        resolve: {
          message: function () {
            return message;
          }
        }
      });

      modalInstance.result.then(function () {
        console.log('Modal closed at: ' + new Date());
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    };

    $scope.submit = function(){
      var complete_url = 'http://intranet.ait.au/developer/emit/services/delete_action.cfm?key=3m1tr0b0trun';

      var requestData = {
        id: $scope.id,
        type: $scope.type,
        reference_po_customer: $scope.reference_po_customer,
        reference: $scope.reference,
        closure_details: $scope.closure_details,
        email_customer: $scope.email_customer,
        notify_internal_person: $scope.notify_internal_person
      };
      $http.post(complete_url,
          $.param(requestData),
          {
            headers:
            {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
          }
      ).
          success(function($data, $status){
            console.log($data);
            if($.trim($data) == 'success' ){
              open('Item has been deleted successfully.');
              $location.path('/details/' + $scope.id);
            }
          }).
          error(function(data, status) {
            $scope.data = data || "Request failed";
            $scope.status = status;
          })
    };
}])
.controller('ForwardController', ['$scope', '$route', '$routeParams', '$location','$http','$templateCache','$modal','$cookies','employeeFactory','emitFactory',
  function($scope, $route, $routeParams, $location,$http,$templateCache,$modal,$cookies,employeeFactory,emitFactory) {
}])
.controller('LinkController', ['$scope', '$route', '$routeParams', '$location','$http','$templateCache','$modal','$cookies','employeeFactory','emitFactory',
  function($scope, $route, $routeParams, $location,$http,$templateCache,$modal,$cookies,employeeFactory,emitFactory) {
}])
.controller('NoteController', ['$scope', '$route', '$routeParams', '$location','$http','$templateCache','$modal','$cookies','employeeFactory','emitFactory',
  function($scope, $route, $routeParams, $location,$http,$templateCache,$modal,$cookies,employeeFactory,emitFactory) {
}])
.controller('QuoteController', ['$scope', '$route', '$routeParams', '$location','$http','$templateCache','$modal','$cookies','employeeFactory','emitFactory',
  function($scope, $route, $routeParams, $location,$http,$templateCache,$modal,$cookies,employeeFactory,emitFactory) {
  }])
.controller('ReplyController', ['$scope', '$route', '$routeParams', '$location','$http','$templateCache','$modal','$cookies','employeeFactory','emitFactory',
  function($scope, $route, $routeParams, $location,$http,$templateCache,$modal,$cookies,employeeFactory,emitFactory) {
  }])
.controller('UrgentController', ['$scope', '$route', '$routeParams', '$location','$http','$templateCache','$modal','$cookies','employeeFactory','emitFactory',
  function($scope, $route, $routeParams, $location,$http,$templateCache,$modal,$cookies,employeeFactory,emitFactory) {
}]);
}).
call(this);



