(function() {
  'use strict';
angular.module('app.list', ['ngRoute','angular-loading-bar','ngCookies'])

.controller('ListController', function($scope, $route, $routeParams, $location,$http,$templateCache,$filter,$cookies) {
  $scope.$route = $route;
  $scope.$location = $location;
  $scope.$routeParams = $routeParams;
  $scope.status = $routeParams.status;
  $scope.bu = $routeParams.bu;
  if(angular.isDefined($routeParams.status)){
  	$scope.status = $routeParams.status;
  }
  else{
  	$scope.status = "";
  }
  if(angular.isDefined($routeParams.group)){
  	$scope.group = $routeParams.group;
  }
  else{
  	$scope.group = "";
  }
  
  if(angular.isDefined($routeParams.owned_by_userno)){
  	$scope.owned_by_userno = $routeParams.owned_by_userno;
  }
  else{
  	$scope.owned_by_userno = "";
  }
  
  if(angular.isDefined($routeParams.bu)){
  	$scope.bu = $routeParams.bu;
  }
  else{
  	$scope.bu = "";
  }
  
  $scope.url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmitList?status=' + $scope.status + '&bu=' + $scope.bu + '&email_pile=' + $scope.group + '&owned_by_userno=' + $scope.owned_by_userno + '&key=3m1tr0b0trun';
  if($cookies.mode === 'test'){
    if($scope.status === 'new')
      $scope.url = "ajax_data/emitnew.json";
    else{
      $scope.url = "ajax_data/emitlist.json";
    }
  }
  $scope.method = 'GET';
  
  $scope.code = null;
  $scope.response = null;
	$http({method: $scope.method, url: $scope.url, cache: $templateCache}).
  success(function(data, status) {
    $scope.status = status;
    $scope.data = data;
    return init();
  }).
  error(function(data, status) {
    $scope.data = data || "Request failed";
    $scope.status = status;
  });
	
	var init;
  $scope.searchKeywords = '';
  $scope.row = '';
  $scope.select = function(page) {
    var end, start;
    start = (page - 1) * $scope.numPerPage;
    end = start + $scope.numPerPage;
    return $scope.currentPageData = $scope.data.slice(start, end);
  };
  
  $scope.onFilterChange = function() {
    $scope.select(1);
    $scope.currentPage = 1;
    return $scope.row = '';
  };
  $scope.onNumPerPageChange = function() {
    console.log($scope.numPerPage);
    $scope.select(1);
    return $scope.currentPage = 1;
  };
  $scope.onOrderChange = function() {
    $scope.select(1);
    return $scope.currentPage = 1;
  };
  
  $scope.numPerPageOpt = [3, 5, 10, 20];
  $scope.numPerPage = $scope.numPerPageOpt[2];
  $scope.currentPage = 1;
  $scope.currentPageData = [];
  init = function() {
    return $scope.select($scope.currentPage);
  };
 })

.controller('DetailsController', ['$scope', '$route', '$routeParams', '$location','$http','$templateCache','$modal','$cookies','employeeFactory','generalAjaxFactory',
function($scope, $route, $routeParams, $location,$http,$templateCache,$modal,$cookies,employeeFactory,generalAjaxFactory) {
  $scope.$route = $route;
  $scope.$location = $location;
  $scope.$routeParams = $routeParams;
  $scope.options = null;
  if(angular.isDefined($routeParams.id)){
  	$scope.id = $routeParams.id;
  }
  else{
  	$scope.id = 0;
  }
  
  var url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmitData?id=' + $scope.id + '&key=3m1tr0b0trun';
	
  $scope.method = 'GET';
  $scope.code = null;
  $scope.response = null;
  var init;
  var details_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmitData?id=' + $scope.id + '&mode=plaintext&key=3m1tr0b0trun';
  var logs_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmitLogData?id=' + $scope.id + '&key=3m1tr0b0trun';
  var files_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmitAttachmentList?id=' + $scope.id + '&key=3m1tr0b0trun';
  var vendors_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getVendors?&key=3m1tr0b0trun';
  var states_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getStates?&key=3m1tr0b0trun';
  var types_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getTypes?&key=3m1tr0b0trun';
  var allocate_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/allocateEmit?id=' + $scope.id + '&key=3m1tr0b0trun';

  if($cookies.mode === 'test'){
    details_url = 'ajax_data/emitdata.json';
    logs_url = 'ajax_data/logs.json';
    files_url = 'ajax_data/attachments.json';
    vendors_url = 'ajax_data/vendors.json';
    states_url = 'ajax_data/states.json';
    types_url = 'ajax_data/types.json';
    allocate_url = 'ajax_data/allocate.html';
  }
  $scope.showplain = function(){
    details_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmitData?id=' + $scope.id + '&mode=plaintext&key=3m1tr0b0trun';
    if($cookies.mode === 'test'){
      details_url = 'ajax_data/emitdataplain.json';
    }
    return init();
  };
  
  $scope.showhtml = function(){
    details_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmitData?id=' + $scope.id + '&key=3m1tr0b0trun';
    if($cookies.mode === 'test'){
      details_url = 'ajax_data/emitdata.json';
    }
    return init();
  };
  
  $scope.allocate = function(){
  	console.log($scope.options.state);
  	var requestData = {
		  id: $scope.id,
		  state: $scope.options.state,
		  vendor: $scope.options.vendor,
		  type: $scope.options.type,
		  transfer_to: $scope.options.owned_by_userno,
		  transfer_reason: $scope.transfer_reason
		};
  	$http.post(allocate_url, 
  	  $.param(requestData),
		  {
      headers:
      {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
      }
	).
	success(function($data, $status){
		console.log($data);
	    if($.trim($data) == 'success' ){
	      open('Item has been allocated successfully.');
	      return init();
	    }
	  }).
    error(function(data, status) {
      $scope.data = data || "Request failed";
      $scope.status = status;
    })
  };
  
  var open = function (message) {
    var modalInstance = $modal.open({
      templateUrl: 'modalContent.html',
      controller: 'ModalInstanceCtrl',
      resolve: {
        message: function () {
          return message;
        }
      }
    });
	
    modalInstance.result.then(function () {
      console.log('Modal closed at: ' + new Date());
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };
  
  $scope.open = open;
  
  var getAllocateData = function(){
  	generalAjaxFactory.getDataWithCache(types_url).then(
  	  function(data){
        $scope.types = data.types.split(",");
      },
      function(errorMessage){
        $scope.error=errorMessage;
      }
    );
    
    generalAjaxFactory.getDataWithCache(vendors_url).then(
  	  function(data){
        $scope.vendors = data.vendors.split(",");
      },
      function(errorMessage){
        $scope.error=errorMessage;
      }
    );
    
    generalAjaxFactory.getDataWithCache(states_url).then(
  	  function(data){
        $scope.states = data.states.split(",");
      },
      function(errorMessage){
        $scope.error=errorMessage;
      }
    );
    
    employeeFactory.getEmployees().then(function(data){
      $scope.employees = data;
    },
    function(errorMessage){
      $scope.error=errorMessage;
    });
  }
  
  init = function(){
  	generalAjaxFactory.getData(details_url).then(
  	  function(data){
        $scope.data = data[0];
        if($scope.data.vendor == 'AVNET TP'){
	    	$scope.data.vendor = 'Avnet TP';
	    }
	    $scope.options = {"type":$scope.data.email_pile,"vendor":$scope.data.vendor,"state":$scope.data.state,"owned_by_userno":$scope.data.owned_by_userno};
      },
      function(errorMessage){
        $scope.error=errorMessage;
      }
    );
    
    getAllocateData();
    
    generalAjaxFactory.getData(logs_url).then(
  	  function(data){
        $scope.logs = data;
      },
      function(errorMessage){
        $scope.error=errorMessage;
      }
    );
    
  	generalAjaxFactory.getData(files_url).then(
  	  function(data){
        $scope.files = data;
      },
      function(errorMessage){
        $scope.error=errorMessage;
      }
    );
  };
  return init();
	
 }]);
}).
call(this);



