(function() {
  'use strict';
  angular.module('app.auth',[]).
  factory('$logincheck', function(){
    return function(userid){
    //Perform logical user loging check either by looking at cookies or make a call to server
    if(userid > 0) return true;
    return false; 
    };
  })
  .controller('loginCtrl', ['$scope', '$route', '$routeParams','$cookies','generalAjaxFactory',function($scope, $route, $routeParams,$cookies,generalAjaxFactory) {
  	$cookies.USERNO = 912;
    $cookies.mode = 'test';
  	//console.log($cookies)
  	if(!angular.isDefined($cookies.USERNO)){
      $location.path('/pages/unauthed');
    }
    else{
      $scope.userno = $cookies.USERNO;  
     // to do - need to work out how to avoid querying my emit data for every page
     //if(!angular.isDefined($cookies.my_emit_data)){

	      $scope.url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmitList?owned_by_userno=' + $scope.userno + '&key=3m1tr0b0trun';
          if($cookies.mode === 'test'){
            $scope.url = 'ajax_data/myemit.json';
          }
          $scope.method = 'GET';
          generalAjaxFactory.getData($scope.url).then(
            function(data){
                $scope.my_emit_data = data;
            },
            function(errorMessage){
                $scope.error=errorMessage;
            }
          );
      //}
      //else{
      	//$scope.my_emit_data = $cookies.my_emit_data;
      //}
      //console.log($cookies.my_emit_data);
    }
 }]);
}).call(this);