(function() {
  'use strict';
  angular.module('app.emit.services',[])
  .factory('employeeFactory', ['$http','$templateCache','$q','$cookies',function($http,$templateCache,$q,$cookies) {
  	return {
  	  //apiPath: 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmployees?&key=3m1tr0b0trun',
  	  apiPath: 'http://intranet.ait.au/developer/emit/services/employees.cfm?key=3m1tr0b0trun',
      test_apiPath: 'ajax_data/employees.json',
  	  getEmployees: function(){
  	  	//Creating a deferred object
        var deferred = $q.defer();
        var url = "";
        if($cookies.mode === 'test'){
          url = this.test_apiPath;
        }
        else{
          url = this.apiPath;
        }
        //Calling Web API to fetch employees
        $http({method: 'GET', url: url, cache: $templateCache}).success(function(data){
          //Passing data to deferred's resolve function on successful completion
          deferred.resolve(data);
        }).error(function(){
 
        //Sending a friendly error message in case of failure
          deferred.reject("An error occured while fetching items");
        });
        
  	  	//Returning the promise object
    	return deferred.promise;
  	  }
  	}
  }])
  .factory('dashboardFactory', ['$http','$templateCache','$q','$cookies',function($http,$templateCache,$q,$cookies) {
      return {
          apiPath: 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getDashboard?key=3m1tr0b0trun',
          test_apiPath: 'ajax_data/dashboard.json',
          getDashboard: function(){
              //Creating a deferred object
              var deferred = $q.defer();
              var url = "";
              if($cookies.mode === 'test'){
                  url = this.test_apiPath;
              }
              else{
                  url = this.apiPath;
              }
              //Calling Web API to fetch employees
              $http({method: 'GET', url: url, cache: $templateCache}).success(function(data){
                  //Passing data to deferred's resolve function on successful completion
                  deferred.resolve(data);
              }).error(function(){

                  //Sending a friendly error message in case of failure
                  deferred.reject("An error occured while fetching items");
              });

              //Returning the promise object
              return deferred.promise;
          }
      }
  }])
  .factory('emitFactory', ['$http','$templateCache','$q',function($http,$templateCache,$q) {
  	return {
  	  getLinks: function(id){
  	  	//Creating a deferred object
        var deferred = $q.defer();
 		var links_url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getEmitLinkList?id='+ id +'&key=3m1tr0b0trun';
        //Calling Web API to fetch employees
        $http({method: 'GET', url: links_url, cache: $templateCache}).success(function(data){
          //Passing data to deferred's resolve function on successful completion
          deferred.resolve(data);
        }).error(function(){
 
        //Sending a friendly error message in case of failure
          deferred.reject("An error occured while fetching items");
        });
        
  	  	//Returning the promise object
    	return deferred.promise;
  	  }
  	}
  }])
  .factory('generalAjaxFactory', ['$http','$templateCache','$q',function($http,$templateCache,$q) {
  	return {
  	  getData: function(url){
  	  	//Creating a deferred object
        var deferred = $q.defer();
 		var links_url = url;
        //Calling Web API to fetch data
        $http({method: 'GET', url: links_url}).success(function(data){
          //Passing data to deferred's resolve function on successful completion
          deferred.resolve(data);
        }).error(function(){
 
        //Sending a friendly error message in case of failure
          deferred.reject("An error occured while fetching items");
        });
        
  	  	//Returning the promise object
    	return deferred.promise;
  	  },
      getDataWithCache: function(url){
        //Creating a deferred object
        var deferred = $q.defer();
        var links_url = url;
        //Calling Web API to fetch data
        $http({method: 'GET', url: links_url, cache: $templateCache}).success(function(data){
            //Passing data to deferred's resolve function on successful completion
            deferred.resolve(data);
        }).error(function(){

            //Sending a friendly error message in case of failure
            deferred.reject("An error occured while fetching items");
        });

        //Returning the promise object
        return deferred.promise;
      }
  	}
  }])
  
 .controller('ModalInstanceCtrl', ['$scope','$modalInstance','message',function ($scope, $modalInstance, message) {
	
  $scope.message = message;

  console.log("msg=" + message);
  $scope.ok = function () {
    $modalInstance.close();
  };

  }])
  ;
}).call(this);