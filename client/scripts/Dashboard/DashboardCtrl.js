(function() {
  'use strict';
angular.module('app.dashboard', [])

.controller('DashboardCtrl', ['$scope','$http','$templateCache','$filter','$cookies','dashboardFactory',function($scope,$http,$templateCache,$filter,$cookies,dashboardFactory) {
    $scope.url = 'http://intranet.ait.au/developer/emitrobot/index.cfm/process/getDashboard?key=3m1tr0b0trun';
    $scope.method = 'GET';
    
    $scope.code = null;
    $scope.response = null;
    dashboardFactory.getDashboard().then(
        function(data){
            $scope.data = data;
        },
        function(errorMessage){
            $scope.error=errorMessage;
        }
    );

 }])
}).call(this);

